package cz.nekola.life

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class LibrarySpec extends FunSpec with Matchers with Checkers {
    describe("library") {
      it("someLibraryMethod is always true") {
        def library = new Library()
        assert(library.someLibraryMethod)
      }
    }
}
