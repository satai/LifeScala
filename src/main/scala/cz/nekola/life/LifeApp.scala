package cz.nekola.life

import scala.io.Source.stdin

object LifeApp {
  def main(args: Array[String]) {

    val input = Console.in.lines.toArray.mkString("\n")

    println(input)
    println("---")
    println(input)
    println("---")
    print(input)

  }
}
