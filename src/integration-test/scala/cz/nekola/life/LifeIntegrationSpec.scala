package cz.nekola.life

import java.io.CharArrayReader

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class LifeIntegrationSpec extends FunSpec with Matchers with Checkers {
    describe("life application") {
      it("outputs series of grids for a given input") {

        val INPUT =
          """
            | oo
            | oo
            |
          """.stripMargin

        val EXPECTED_OUTPUT =
          s"""$INPUT
             |---
             |$INPUT
             |---
             |$INPUT""".stripMargin

        val output = new java.io.ByteArrayOutputStream()
        val input = new CharArrayReader(INPUT.toCharArray)

        Console.withIn(input) {
          Console.withOut(output) {
            LifeApp.main(Array())
          }
        }
        assert(EXPECTED_OUTPUT == output.toString)
      }
    }
}
